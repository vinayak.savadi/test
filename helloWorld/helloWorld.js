import { LightningElement,track,api } from 'lwc';
    
export default class HelloWorld extends LightningElement {
        @track startDate;
        @track endDate;
        @track greeting = 'World';
        @track todayDate = new Date();
        days = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
        @track view = {
          // View Select
          options: [
            {
              label: "View by Day",
              value: "1/14"
            },
            {
              label: "View by hours",
              value: "7/10"
            }
          ],
          slotSize: 1,
          slots: 1
        };

        @track techList = new Array(
          {
          
              Name  : "Chris Woaks",
              Skill : "4",
              Id : '2345' 
          },
          {
          
            Name  : "John Baley",
            Skill : "2",
            Id : '2311' 
        }
        
       );
       
        mapWODetails = new Map();
        //mapWODetails = [];
        // @track startDate ;
       
        connectedCallback() {
           
            this.startDate = new Date();
            this.getDates( this.startDate);
        }

         allowDrop(evt) {
            // eslint-disable-next-line no-console
            //console.log('entered allow drop');
            evt.preventDefault();
            evt.stopPropagation();
          }

          drag(evt) {
            // eslint-disable-next-line @lwc/lwc/no-inner-html
            //eslint-disable-next-line @lwc/lwc/no-inner-html
            evt.dataTransfer.setData("woID",  evt.target.innerHTML);
            evt.dataTransfer.setData("workOrder",  evt.target.dataset.targetId);
            evt.stopPropagation();
          }
          
        @api cellClicked ;
        drop(evt) {
            evt.preventDefault();
            //this.cellClicked = true;
            // eslint-disable-next-line no-console
            console.log('Elemet droped');
            // eslint-disable-next-line @lwc/lwc/no-inner-html 
            // eslint-disable-next-line @lwc/lwc/no-inner-html
            evt.target.innerHTML = evt.dataTransfer.getData('woID');
            this.mapWODetails.set(evt.target.rowSpan,evt.target.dataset.targetId);
            //evt.target.appendChild();
            evt.stopPropagation();
           
          }
          
        
       
        @track dateArray;
        getDates(startDate) {
            // eslint-disable-next-line no-console
         
            var arr = new Array();      
            var nextDate = new Date(startDate);
            var day;
           // var disDate;
            var dateVal;
            var isHoliday = false;
            var i = 0;
            // eslint-disable-next-line no-console
            console.log('came in getdates');
            
            while (arr.length < 8) {
                nextDate = new Date(nextDate.getFullYear(), nextDate.getMonth(), nextDate.getDate());              
                day = this.days[nextDate.getDay()];
                if (i === 7) {
                    this.endDate = nextDate;
                }
                dateVal = new Date(nextDate.getTime() - (nextDate.getTimezoneOffset() * 60000 )).toISOString().split("T")[0];
                //disDate = nextDate.toLocaleDateString('en-US', { month: 'numeric', day: 'numeric' });
               // isHoliday = this.checkHoliday(dateVal);
               if(day !== 'SUN' && day !== 'SAT'){
                    arr.push({
                        date: nextDate.getDate() + '/' + (nextDate.getMonth()+1) + '/' + nextDate.getFullYear() ,
                        day: day,
                        dateVal: dateVal,
                        isHoliday : isHoliday                   
                    });
                }
                nextDate = new Date(nextDate.getFullYear(), nextDate.getMonth(), nextDate.getDate() + 1);
                i++;
            } 
            this.endDate  = (arr[arr.length-1].dateVal);    
            this.dateArray = arr;       
           // fireEvent(this.pageRef, 'stopSpinner', this);
        }

       
        @api targetColSpan;
        
        extendColSpan(evt){
         
         // if(evt.target.dataset.targetId ){

          let targetId = evt.target.dataset.targetId;
          window.console.log('clicked >>' + targetId);
          let startIndex = this.mapWODetails.get(evt.target.rowSpan);
          window.console.log('evt.target.colspan>>' + startIndex);
          this.template.querySelectorAll(`[data-id="${startIndex}"]`)[1].colSpan = (parseInt(this.template.querySelectorAll(`[data-id="${startIndex}"]`)[1].colSpan,10) + 1);
          
          //evt.target.colSpan = element;
          window.console.log('evt.target.colspan>>' + this.template.querySelectorAll(`[data-id="${startIndex}"]`)[1].colSpan);
         // evt.stopPropagation();
          //this.template.querySelector(`[data-id="${targetId}"]`).colSpan = this.targetColSpan;
         // }
        }

        handleViewChange(event) {
          this.setView(event.target.value);
          //this.setDateHeaders();
          //this.handleRefresh();
        }

        navigateToNext(){
          this.getDates(this.endDate);
        }

        
    }